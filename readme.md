# Disable Interfaces on Raspberry OS lite

- disable wifi, bluetooth, audio, video and leds

## Requirements

- Raspberry Pi 3B/4B (tested)
- Raspian OS lite (bullseye, bookworm)
- Kernel Version 5.18+
- [Video Tutorial](https://youtube.com/shorts/aw3y8GdFddc)
- [config.txt](https://www.raspberrypi.com/documentation/computers/config_txt.html)
- [overlays](https://github.com/raspberrypi/firmware/blob/master/boot/overlays/README)

## Introductions

- step 1 | open config file
```bash
sudo nano /boot/firmware/config.txt

# or

sudo nano /boot/config.txt
```
- step 2 | disable interfaces
```bash
# disable analog audio
dtparam=audio=off

# disable audio via hdmi
dtoverlay=vc4-kms-v3d,noaudio

# insert next lines at the end of the file
[my-config]

# disable hdmi
hdmi_blanking=1

# disable wifi
dtoverlay=disable-wifi

# disable bluetooth
dtoverlay=disable-bt

# turn off activity led
dtparam=act_led_trigger=none
dtparam=act_led_activelow=off

# turn off power led (raspi 3)
dtparam=pwr_led_trigger=none
dtparam=pwr_led_activelow=off

# turn off power led (raspi 4)
dtparam=pwr_led_trigger=default-on
dtparam=pwr_led_activelow=off

# turn off ethernet act,lnk led (raspi 3B)
sudo apt install libusb-1.0-0-dev

https://github.com/dumpsite/lan951x-led-ctl.git

# turn off ethernet act led (raspi 3B+)
dtparam=eth_led0=14

# turn off ethernet lnk led (raspi 3B+)
dtparam=eth_led1=14

# turn off ethernet act led (raspi 4)
dtparam=eth_led0=4

# turn off ethernet lnk led (raspi 4)
dtparam=eth_led1=4
```
- step 3 | reboot system
```bash
sudo reboot
```
